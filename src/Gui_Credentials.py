from tkinter import *
from tkinter import ttk
import sqlite3

f1="../data/DRM.db"
f2="../data/Log.db"

def show():
    tl2 = Toplevel()
    tl2.title("Show Database")
    tl2.geometry("1024x720")
    conn  = sqlite3.connect(f1)
    c = conn.cursor()

    style = ttk.Style()
    style.theme_use('default')
    style.configure("Treeview",
        background="#D3D3D3",
        foreground="black",
        rowheight=30,
        fieldbackground="#D3D3D3")
    style.map("Treeview",
             background=[("selected","#347083")])
    
    tree_frame = Frame(tl2)
    tree_frame.pack(pady=10,side=TOP,fill=X)

    tree_scroll_v = Scrollbar(tree_frame)
    tree_scroll_h = Scrollbar(tree_frame, orient=HORIZONTAL)
    tree_scroll_v.pack(side=RIGHT,fill=Y)
    tree_scroll_h.pack(side=BOTTOM,fill=X)

    my_tree = ttk.Treeview(tree_frame, yscrollcommand=tree_scroll_v.set, xscrollcommand=tree_scroll_h.set)
    my_tree.pack()

    tree_scroll_v.config(command=my_tree.yview)
    tree_scroll_h.config(command=my_tree.xview)

    my_tree['columns'] = ("AMS_ID","AMS_Status","Name","DOB","Emp_ID","Designation", "Office", "Facility_ID", "MDM_ID", 
                        "Reporting_office","Sub_Division", "Retirement_Date", "Phone_asset_id", "P_IMEI_1", "P_IMEI_2", "P_Model", "Biometric_asset_id", 
                        "Serial_No", "B_Model", "Sim_No", "Mobile_No", "Brand")
    
    my_tree.column("#0", width="0", stretch=NO)
    my_tree.column("AMS_ID", width=125)
    my_tree.column("AMS_Status", width=125)
    my_tree.column("Name", width=125)
    my_tree.column("DOB", width=125)
    my_tree.column("Emp_ID", width=125)
    my_tree.column("Designation", width=125)
    my_tree.column("Office", width=125)
    my_tree.column("Facility_ID", width=125)
    my_tree.column("MDM_ID", width=125)
    my_tree.column("Reporting_office", width=125)
    my_tree.column("Sub_Division", width=125)
    my_tree.column("Retirement_Date", width=125)
    my_tree.column("Phone_asset_id", width=125)
    my_tree.column("P_IMEI_1", width=125)
    my_tree.column("P_IMEI_2", width=125)
    my_tree.column("P_Model", width=125)
    my_tree.column("Biometric_asset_id", width=125)
    my_tree.column("Serial_No", width=125)
    my_tree.column("B_Model", width=125)
    my_tree.column("Sim_No", width=125)
    my_tree.column("Mobile_No", width=125)
    my_tree.column("Brand", width=125)

    my_tree.heading("#0", text="")
    my_tree.heading("AMS_ID", text="AMS_ID")
    my_tree.heading("AMS_Status", text="AMS_Status")
    my_tree.heading("Name", text="Name")
    my_tree.heading("DOB", text="DOB")
    my_tree.heading("Emp_ID", text="Emp_ID")
    my_tree.heading("Designation", text="Designation")
    my_tree.heading("Office", text="Office")
    my_tree.heading("Facility_ID", text="Facility_ID")
    my_tree.heading("MDM_ID", text="MDM_ID")
    my_tree.heading("Reporting_office", text="Reporting_office")
    my_tree.heading("Sub_Division", text="Sub_Division")
    my_tree.heading("Retirement_Date", text="Retirement_Date")
    my_tree.heading("Phone_asset_id", text="Phone_asset_id")
    my_tree.heading("P_IMEI_1", text="P_IMEI_1")
    my_tree.heading("P_IMEI_2", text="P_IMEI_2")
    my_tree.heading("P_Model", text="P_Model")
    my_tree.heading("Biometric_asset_id", text="Biometric_asset_id")
    my_tree.heading("Serial_No", text="Serial_No")
    my_tree.heading("B_Model", text="B_Model")
    my_tree.heading("Sim_No", text="Sim_No")
    my_tree.heading("Mobile_No", text="Mobile_No")
    my_tree.heading("Brand", text="Brand")

    my_tree.tag_configure('oddrow', background="white")
    my_tree.tag_configure('evenrow', background="lightblue")

    c.execute("""SELECT Employee_info.AMS_ID, Employee_info.AMS_Status, Employee_info.Name, Employee_info.DOB, Employee_info.Emp_ID, Employee_info.Designation, 
                Employee_info.Office, Employee_info.Facility_ID, Employee_info.MDM_ID, Employee_info.Reporting_office, Employee_info.Sub_Division, Employee_info.Retirement_Date, 
                Employee_info.Phone_asset_id, Device_phone.IMEI_1, Device_phone.IMEI_2, Device_phone.Model, Employee_info.Biometric_asset_id, Device_biometric.Serial_No, Device_biometric.Model,
                Employee_info.Sim_No, Sim.Mobile_No, Sim.Brand
                FROM Employee_info
                LEFT JOIN Device_phone on Employee_info.Phone_asset_id = Device_phone.Asset_ID
                LEFT JOIN Device_biometric on Employee_info.Biometric_asset_id = Device_biometric.Asset_ID
                LEFT JOIN Sim on Employee_info.Sim_No = Sim.Sim_No""")
    sql_result = c.fetchall()

    global count
    count = 0

    for record in sql_result:
        if count % 2 ==0:
            my_tree.insert(parent='', index='end', iid=count, text='', values=(record[0],record[1],record[2],record[3],record[4],record[5],record[6],record[7],
            record[8],record[9],record[10],record[11],record[12],record[13],record[14],record[15],record[16],record[17],record[18],record[19],
            record[20],record[21]), tags=('evenrow',))
        else:
            my_tree.insert(parent='', index='end', iid=count, text='', values=(record[0],record[1],record[2],record[3],record[4],record[5],record[6],record[7],
            record[8],record[9],record[10],record[11],record[12],record[13],record[14],record[15],record[16],record[17],record[18],record[19],
            record[20],record[21]), tags=('oddrow',))
        count +=1
    conn.commit()
    conn.close()
    
    def add_c():

        # creating the top level tl3
        tl3 = Toplevel()
        tl3.title("Insert Credentials")
        tl3.geometry("1024x720")
        tl3.rowconfigure(0, weight=1)
        tl3.columnconfigure(0, weight=1)
        tl3.columnconfigure(1,weight=1)
        tl3.columnconfigure(2,weight=1)
        tl3.columnconfigure(3,weight=1)

        e_frame = Frame(tl3)
        e_frame.grid(row=0,column=0, sticky="ns",padx=10)

        p_frame = Frame(tl3)
        p_frame.grid(row=0,column=1, sticky="ns",padx=10)
        
        b_frame = Frame(tl3)
        b_frame.grid(row=0,column=2, sticky="ns",padx=10)

        s_frame = Frame(tl3)
        s_frame.grid(row=0,column=3, sticky="ns",padx=10)

        elabel1 = Label(e_frame,text="AMS_ID")
        elabel2 = Label(e_frame,text="AMS_Status")
        elabel3 = Label(e_frame,text="Name")
        elabel4 = Label(e_frame,text="DOB")
        elabel5 = Label(e_frame,text="Emp_ID")
        elabel6 = Label(e_frame,text="Designation")
        elabel7 = Label(e_frame,text="Office")
        elabel8 = Label(e_frame,text="Facility_ID")
        elabel9 = Label(e_frame,text="MDM_ID")
        elabel10 = Label(e_frame,text="Reporting_office")
        elabel11 = Label(e_frame,text="Sub_Division")
        elabel12 = Label(e_frame,text="Retirement_Date")
        

        elabel1.grid(row=0,column=0,pady=10)
        elabel2.grid(row=1,column=0,pady=10)
        elabel3.grid(row=2,column=0,pady=10)
        elabel4.grid(row=3,column=0,pady=10)
        elabel5.grid(row=4,column=0,pady=10)
        elabel6.grid(row=5,column=0,pady=10)
        elabel7.grid(row=6,column=0,pady=10)
        elabel8.grid(row=7,column=0,pady=10)
        elabel9.grid(row=8,column=0,pady=10)
        elabel10.grid(row=10,column=0,pady=10)
        elabel11.grid(row=11,column=0,pady=10)
        elabel12.grid(row=12,column=0,pady=10)
        
        eentry1 = Entry(e_frame, width=50)
        eentry2 = Entry(e_frame, width=50)
        eentry3 = Entry(e_frame,width=50)
        eentry4 = Entry(e_frame,width=50)
        eentry5 = Entry(e_frame,width=50)
        eentry6 = Entry(e_frame,width=50)
        eentry7 = Entry(e_frame,width=50)
        eentry8 = Entry(e_frame,width=50)
        eentry9 = Entry(e_frame,width=50)
        eentry10 = Entry(e_frame,width=50)
        eentry11 = Entry(e_frame,width=50)
        eentry12 = Entry(e_frame,width=50)
        
        eentry1.grid(row=0,column=1,pady=10)
        eentry2.grid(row=1,column=1,pady=10)
        eentry3.grid(row=2,column=1,pady=10)
        eentry4.grid(row=3,column=1,pady=10)
        eentry5.grid(row=4,column=1,pady=10)
        eentry6.grid(row=5,column=1,pady=10)
        eentry7.grid(row=6,column=1,pady=10)
        eentry8.grid(row=7,column=1,pady=10)
        eentry9.grid(row=8,column=1,pady=10)
        eentry10.grid(row=10,column=1,pady=10)
        eentry11.grid(row=11,column=1,pady=10)
        eentry12.grid(row=12,column=1,pady=10)

        def e_add():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""INSERT INTO Employee_info
                    (AMS_ID, AMS_Status, Name, DOB, Emp_ID, Designation, Office, Facility_ID, MDM_ID, Reporting_office, Sub_Division, Retirement_Date)
                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?)""",(eentry1.get(),eentry2.get(),eentry3.get(),eentry4.get(),eentry5.get(),eentry6.get(),eentry7.get(),eentry8.get(),eentry9.get(),eentry10.get(),eentry11.get(),eentry12.get()))
            conn.commit()
            conn.close()
            eentry1.delete(0,END)
            eentry2.delete(0,END)
            eentry3.delete(0,END)
            eentry4.delete(0,END)
            eentry5.delete(0,END)
            eentry6.delete(0,END)
            eentry7.delete(0,END)
            eentry8.delete(0,END)
            eentry9.delete(0,END)
            eentry10.delete(0,END)
            eentry11.delete(0,END)
            eentry12.delete(0,END)
        
        ebutton = Button(e_frame, text="Add", command=e_add)

        ebutton.grid(row=13,column=0,columnspan=2)

        plabel1 = Label(p_frame, text="Asset ID:")
        plabel2 = Label(p_frame, text="IMEI 1 :")
        plabel3 = Label(p_frame, text="IMEI 2 :")
        plabel4 = Label(p_frame, text="Model :")

        plabel1.grid(row=0,column=0,pady=10)
        plabel2.grid(row=1,column=0,pady=10)
        plabel3.grid(row=2,column=0,pady=10)
        plabel4.grid(row=3,column=0,pady=10)

        pentry1 = Entry(p_frame, width=50)
        pentry2 = Entry(p_frame, width=50)
        pentry3 = Entry(p_frame, width=50)
        pentry4 = Entry(p_frame, width=50)
        
        pentry1.grid(row=0,column=1,pady=10)
        pentry2.grid(row=1,column=1,pady=10)
        pentry3.grid(row=2,column=1,pady=10)
        pentry4.grid(row=3,column=1,pady=10)

        def p_add():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""INSERT INTO Device_phone
                    VALUES (?,?,?,?)""",(pentry1.get(),pentry2.get(),pentry3.get(),pentry4.get()))
            conn.commit()
            conn.close()
            pentry1.delete(0,END)
            pentry2.delete(0,END)
            pentry3.delete(0,END)
            pentry4.delete(0,END)

        pbutton = Button(p_frame, text="Add", command=p_add)

        pbutton.grid(row=4,column=0,columnspan=2)

        blabel1 = Label(b_frame, text="Asset ID:")
        blabel2 = Label(b_frame, text="Serial No :")
        blabel3 = Label(b_frame, text="Model :")
        
        blabel1.grid(row=0,column=0,pady=10)
        blabel2.grid(row=1,column=0,pady=10)
        blabel3.grid(row=2,column=0,pady=10)
        
        bentry1 = Entry(b_frame,width=50)
        bentry2 = Entry(b_frame,width=50)
        bentry3 = Entry(b_frame,width=50)
                
        bentry1.grid(row=0,column=1,pady=10)
        bentry2.grid(row=1,column=1,pady=10)
        bentry3.grid(row=2,column=1,pady=10)

        def b_add():
            conn = sqlite3.connect(f1)
            c =conn.cursor()
            c.execute("""INSERT INTO Device_biometric
                    VALUES (?,?,?)""",(bentry1.get(),bentry2.get(),bentry3.get()))
            conn.commit()
            conn.close()
            bentry1.delete(0,END)
            bentry2.delete(0,END)
            bentry3.delete(0,END)

        bbutton = Button(b_frame, text="Add", command=b_add)

        bbutton.grid(row=4,column=0,columnspan=2)

        slabel1 = Label(s_frame, text="Sim No:")
        slabel2 = Label(s_frame, text="Mobile No :")
        slabel3 = Label(s_frame, text="Brand :")
        
        slabel1.grid(row=0,column=0,pady=10)
        slabel2.grid(row=1,column=0,pady=10)
        slabel3.grid(row=2,column=0,pady=10)
        
        sentry1 = Entry(s_frame,width=50)
        sentry2 = Entry(s_frame,width=50)
        sentry3 = Entry(s_frame,width=50)
                
        sentry1.grid(row=0,column=1,pady=10)
        sentry2.grid(row=1,column=1,pady=10)
        sentry3.grid(row=2,column=1,pady=10)

        def s_add():
            conn = sqlite3.connect(f1)
            c =conn.cursor()
            c.execute("""INSERT INTO Sim
                    VALUES (?,?,?)""",(sentry1.get(),sentry2.get(),sentry3.get()))
            conn.commit()
            conn.close()
            sentry1.delete(0,END)
            sentry2.delete(0,END)
            sentry3.delete(0,END)
        
        sbutton = Button(s_frame, text="Add", command=s_add)

        sbutton.grid(row=3,column=0,columnspan=2)
        tl3.mainloop()

    add = Button(tl2, text="Add Credentials", command=add_c)
    add.pack(pady=10)

    def upd_c():

        tl4 = Toplevel()
        tl4.title("Update Credentials")
        tl4.geometry("1024x720")
        
        e_frame = Frame(tl4)
        e_frame.grid(row=0,column=0, sticky="ns",padx=10)

        selected = my_tree.focus()
        values = my_tree.item(selected, 'values')

        def e_upd():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""UPDATE Employee_info
                    SET AMS_ID = ?,AMS_Status = ?, Name = ?, DOB = ?, Emp_ID = ?, Designation = ?, Office = ?, Facility_ID = ?,
                    MDM_ID = ?, Reporting_office = ?, Sub_Division = ?,Retirement_Date = ?
                    WHERE AMS_ID = ?""",
                    (eentry1.get(),eentry2.get(),eentry3.get(),eentry4.get(),eentry5.get(),
                    eentry6.get(),eentry7.get(),eentry8.get(),eentry9.get(),eentry10.get(),
                    eentry11.get(),eentry12.get(),values[0]))            
            conn.commit()
            conn.close()

            eentry1.delete(0,END)
            eentry2.delete(0,END)
            eentry3.delete(0,END)
            eentry4.delete(0,END)
            eentry5.delete(0,END)
            eentry6.delete(0,END)
            eentry7.delete(0,END)
            eentry8.delete(0,END)
            eentry9.delete(0,END)
            eentry10.delete(0,END)
            eentry11.delete(0,END)
            eentry12.delete(0,END)

        elabel1 = Label(e_frame,text="AMS_ID")
        elabel2 = Label(e_frame,text="AMS_Status")
        elabel3 = Label(e_frame,text="Name")
        elabel4 = Label(e_frame,text="DOB")
        elabel5 = Label(e_frame,text="Emp_ID")
        elabel6 = Label(e_frame,text="Designation")
        elabel7 = Label(e_frame,text="Office")
        elabel8 = Label(e_frame,text="Facility_ID")
        elabel9 = Label(e_frame,text="MDM_ID")
        elabel10 = Label(e_frame,text="Reporting_office")
        elabel11 = Label(e_frame,text="Sub_Division")
        elabel12 = Label(e_frame,text="Retirement_Date")
        

        elabel1.grid(row=0,column=0,pady=10)
        elabel2.grid(row=1,column=0,pady=10)
        elabel3.grid(row=2,column=0,pady=10)
        elabel4.grid(row=3,column=0,pady=10)
        elabel5.grid(row=4,column=0,pady=10)
        elabel6.grid(row=5,column=0,pady=10)
        elabel7.grid(row=6,column=0,pady=10)
        elabel8.grid(row=7,column=0,pady=10)
        elabel9.grid(row=8,column=0,pady=10)
        elabel10.grid(row=9,column=0,pady=10)
        elabel11.grid(row=10,column=0,pady=10)
        elabel12.grid(row=11,column=0,pady=10)
        
        eentry1 = Entry(e_frame, width=50)
        eentry2 = Entry(e_frame, width=50)
        eentry3 = Entry(e_frame,width=50)
        eentry4 = Entry(e_frame,width=50)
        eentry5 = Entry(e_frame,width=50)
        eentry6 = Entry(e_frame,width=50)
        eentry7 = Entry(e_frame,width=50)
        eentry8 = Entry(e_frame,width=50)
        eentry9 = Entry(e_frame,width=50)
        eentry10 = Entry(e_frame,width=50)
        eentry11 = Entry(e_frame,width=50)
        eentry12 = Entry(e_frame,width=50)
        
        eentry1.grid(row=0,column=1,pady=10)
        eentry2.grid(row=1,column=1,pady=10)
        eentry3.grid(row=2,column=1,pady=10)
        eentry4.grid(row=3,column=1,pady=10)
        eentry5.grid(row=4,column=1,pady=10)
        eentry6.grid(row=5,column=1,pady=10)
        eentry7.grid(row=6,column=1,pady=10)
        eentry8.grid(row=7,column=1,pady=10)
        eentry9.grid(row=8,column=1,pady=10)
        eentry10.grid(row=9,column=1,pady=10)
        eentry11.grid(row=10,column=1,pady=10)
        eentry12.grid(row=11,column=1,pady=10)

        eentry1.delete(0,END)
        eentry2.delete(0,END)
        eentry3.delete(0,END)
        eentry4.delete(0,END)
        eentry5.delete(0,END)
        eentry6.delete(0,END)
        eentry7.delete(0,END)
        eentry8.delete(0,END)
        eentry9.delete(0,END)
        eentry10.delete(0,END)
        eentry11.delete(0,END)
        eentry12.delete(0,END)

        eentry1.insert(0, values[0])
        eentry2.insert(0, values[1])
        eentry3.insert(0, values[2])
        eentry4.insert(0, values[3])
        eentry5.insert(0, values[4])
        eentry6.insert(0, values[5])
        eentry7.insert(0, values[6])
        eentry8.insert(0, values[7])
        eentry9.insert(0, values[8])
        eentry10.insert(0, values[9])
        eentry11.insert(0, values[10])
        eentry12.insert(0, values[11])

        
        eupd = Button(e_frame, text="Update", command=e_upd)
        eupd.grid(row=13,column=0,columnspan=2)
        tl4.mainloop()

    def upd_d():
        tl5 = Toplevel()
        tl5.title("Update Device")
        tl5.geometry("1024x720")
        p_frame = Frame(tl5)
        p_frame.pack(pady=10)
        b_frame = Frame(tl5)
        b_frame.pack(pady=10)
        s_frame = Frame(tl5)
        s_frame.pack(pady=10)

        def p_fill():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""SELECT Device_phone.IMEI_1, Device_phone.IMEI_2, Device_phone.Model
                    FROM Device_phone
                    WHERE Device_phone.Asset_ID = ? """,(p_asset_id.get(),))
            p_fill_list = c.fetchall()
            
            p_imei_1.delete(0,END)
            p_imei_2.delete(0,END)
            p_model.delete(0,END)

            for item in p_fill_list:
                p_imei_1.insert(0, item[0])
                p_imei_2.insert(0, item[1])
                p_model.insert(0, item[2])

            conn.commit()
            conn.close()

        def p_upd():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""UPDATE Device_phone
                    SET IMEI_1 = ?, IMEI_2 = ?, Model = ?
                    WHERE Asset_ID = ?""",(p_imei_1.get(),p_imei_2.get(),p_model.get(),p_asset_id.get()))
            conn.commit()
            conn.close()
            p_imei_1.delete(0,END)
            p_imei_2.delete(0,END)
            p_model.delete(0,END)

        p_asset_id_l = Label(p_frame, text="Phone Asset Id:",width=30)
        p_asset_id_l.grid(row=0,column=0)

        p_asset_id = Entry(p_frame, width=50)
        p_asset_id.grid(row=0,column=1)

        p_search = Button(p_frame, text="Search",width=30,command=p_fill)
        p_search.grid(row=0,column=2)

        p_imei_1_l = Label(p_frame, text="IMEI 1", width=30)
        p_imei_1_l.grid(row=1,column=0)

        p_imei_1 = Entry(p_frame, width=50)
        p_imei_1.grid(row=1,column=1)

        p_imei_2_l = Label(p_frame, text="IMEI 2", width=30)
        p_imei_2_l.grid(row=2,column=0)

        p_imei_2 = Entry(p_frame, width=50)
        p_imei_2.grid(row=2,column=1)

        p_model_l = Label(p_frame, text="Model", width=30)
        p_model_l.grid(row=3,column=0)

        p_model = Entry(p_frame, width=50)
        p_model.grid(row=3,column=1)

        pupd = Button(p_frame, text="Update", width=30, command=p_upd)
        pupd.grid(row=4,column=0)

        def b_fill():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""SELECT Device_biometric.Serial_No, Device_biometric.Model
                    FROM Device_biometric
                    WHERE Device_biometric.Asset_ID = ? """,(b_asset_id.get(),))
            b_fill_list = c.fetchall()
            
            b_serial_no.delete(0,END)
            b_model.delete(0,END)

            for item in b_fill_list:                
                b_serial_no.insert(0, item[0])
                b_model.insert(0, item[1])

            conn.commit()
            conn.close()
        
        def b_upd():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""UPDATE Device_biometric
                    SET Serial_No = ?, Model = ?
                    WHERE Asset_ID = ?""",(b_serial_no.get(),b_model.get(),b_asset_id.get()))
            conn.commit()
            conn.close()
            
            b_serial_no.delete(0,END)
            b_model.delete(0,END)

        b_asset_id_l = Label(b_frame, text="Biometric Asset Id:",width=30)
        b_asset_id_l.grid(row=0,column=0)

        b_asset_id = Entry(b_frame, width=50)
        b_asset_id.grid(row=0,column=1)

        b_search = Button(b_frame, text="Search",width=30,command=b_fill)
        b_search.grid(row=0,column=2)

        b_serial_no_l = Label(b_frame, text="Serial No", width=30)
        b_serial_no_l.grid(row=1,column=0)

        b_serial_no = Entry(b_frame, width=50)
        b_serial_no.grid(row=1,column=1)

        b_model_l = Label(b_frame, text="Model", width=30)
        b_model_l.grid(row=2,column=0)

        b_model = Entry(b_frame, width=50)
        b_model.grid(row=2,column=1)

        bupd = Button(b_frame, text="Update", width=30, command=b_upd)
        bupd.grid(row=3,column=0)

        def s_fill():
            conn = sqlite3.connect(f1)
            c = conn.cursor()
            c.execute("""SELECT Sim.Mobile_No, Sim.Brand
                    FROM Sim
                    WHERE Sim.Sim_No = ? """,(s_sim_no.get(),))
            s_fill_list = c.fetchall()
            
            s_mobile_no.delete(0,END)
            s_brand.delete(0,END)

            for item in s_fill_list:                
                s_mobile_no.insert(0, item[0])
                s_brand.insert(0, item[1])

            conn.commit()
            conn.close()
        
        def s_upd():
            conn = sqlite3.connect(f1)
            c = conn.cursor()            
            c.execute("""UPDATE Sim
                    SET Mobile_No = ?, Brand = ?
                    WHERE Sim_No = ?""",(s_mobile_no.get(),s_brand.get(),s_sim_no.get()))
            conn.commit()
            conn.close()
            
            s_mobile_no.delete(0,END)
            s_brand.delete(0,END)

        s_sim_no_l = Label(s_frame, text="Sim No:",width=30)
        s_sim_no_l.grid(row=0,column=0)

        s_sim_no = Entry(s_frame, width=50)
        s_sim_no.grid(row=0,column=1)

        s_search = Button(s_frame, text="Search",width=30,command=s_fill)
        s_search.grid(row=0,column=2)

        s_mobile_no_l = Label(s_frame, text="Mobile No", width=30)
        s_mobile_no_l.grid(row=1,column=0)

        s_mobile_no = Entry(s_frame, width=50)
        s_mobile_no.grid(row=1,column=1)

        s_brand_l = Label(s_frame, text="Brand", width=30)
        s_brand_l.grid(row=2,column=0)

        s_brand = Entry(s_frame, width=50)
        s_brand.grid(row=2,column=1)

        supd = Button(s_frame, text="Update", width=30, command=s_upd)
        supd.grid(row=3,column=0)

        tl5.mainloop()
        
    upd = Button(tl2, text="Update Credentials", command=upd_c)
    upd.pack(pady=10)

    updd = Button(tl2, text="Update Device", command=upd_d)
    updd.pack(pady=10)

    tl2.mainloop()

def log():
    tl6 = Toplevel()
    tl6.title("Show Database")
    tl6.geometry("1024x720")
    conn  = sqlite3.connect(f2)
    c = conn.cursor()
    style = ttk.Style()
    style.theme_use('default')
    style.configure("Treeview",
        background="#D3D3D3",
        foreground="black",
        rowheight=30,
        fieldbackground="#D3D3D3")
    style.map("Treeview",
             background=[("selected","#347083")])
    
    tree_frame1 = Frame(tl6)
    tree_frame1.pack(pady=10, side=TOP,fill=X)

    tree_scroll1_v = Scrollbar(tree_frame1)
    tree_scroll1_v.pack(side=RIGHT,fill=Y)

    my_tree1 = ttk.Treeview(tree_frame1, yscrollcommand=tree_scroll1_v.set)
    my_tree1.pack()

    tree_scroll1_v.config(command=my_tree1.yview)

    my_tree1['columns'] = ("Reciept_No", "Date", "AMS_ID", "Asset_id", "Device")
    my_tree1.column("#0", width="0", stretch=NO)
    my_tree1.column("Reciept_No", width=125)
    my_tree1.column("Date", width=125)
    my_tree1.column("AMS_ID", width=125)
    my_tree1.column("Asset_id", width=125)
    my_tree1.column("Device", width=125)

    my_tree1.heading("#0", text="")
    my_tree1.heading("Reciept_No", text="Reciept_No")
    my_tree1.heading("Date", text="Date")
    my_tree1.heading("AMS_ID", text="AMS_ID")
    my_tree1.heading("Asset_id", text="Asset_id")
    my_tree1.heading("Device", text="Device")

    my_tree1.tag_configure('oddrow', background="white")
    my_tree1.tag_configure('evenrow', background="lightblue")

    c.execute("""SELECT Provider_Log.Reciept_no, Provider_Log.Date, Provider_Log.AMS_ID, Provider_Log.Asset_id, Provider_Log.Device
            FROM Provider_Log""")
    result1 = c.fetchall()
    global count
    count = 0
    for item in result1:
        if count%2 ==0:
            my_tree1.insert(parent="", index=END, iid=count, text="", values=(item[0],item[1],item[2],item[3],item[4]),tags=("evenrow",))
        else:
            my_tree1.insert(parent="", index=END, iid=count, text="", values=(item[0],item[1],item[2],item[3],item[4]),tags=("oddrow",))
        count +=1
    
    tree_frame2 = Frame(tl6)
    tree_frame2.pack(pady=10, side=TOP,fill=X)

    tree_scroll2_v = Scrollbar(tree_frame2)
    tree_scroll2_v.pack(side=RIGHT,fill=Y)

    my_tree2 = ttk.Treeview(tree_frame2, yscrollcommand=tree_scroll2_v.set)
    my_tree2.pack()

    tree_scroll2_v.config(command=my_tree2.yview)

    my_tree2['columns'] = ("Reciept_No", "Date", "AMS_ID", "Asset_id", "Device")
    my_tree2.column("#0", width="0", stretch=NO)
    my_tree2.column("Reciept_No", width=125)
    my_tree2.column("Date", width=125)
    my_tree2.column("AMS_ID", width=125)
    my_tree2.column("Asset_id", width=125)
    my_tree2.column("Device", width=125)

    my_tree2.heading("#0", text="")
    my_tree2.heading("Reciept_No", text="Reciept_No")
    my_tree2.heading("Date", text="Date")
    my_tree2.heading("AMS_ID", text="AMS_ID")
    my_tree2.heading("Asset_id", text="Asset_id")
    my_tree2.heading("Device", text="Device")

    my_tree2.tag_configure('oddrow', background="white")
    my_tree2.tag_configure('evenrow', background="lightblue")

    c.execute("""SELECT Retrieval_Log.Reciept_no, Retrieval_Log.Date, Retrieval_Log.AMS_ID, Retrieval_Log.Asset_id, Retrieval_Log.Device
            FROM Retrieval_Log""")
    result2 = c.fetchall()
    count = 0
    for item in result2:
        if count%2 ==0:
            my_tree2.insert(parent="", index=END, iid=count, text="", values=(item[0],item[1],item[2],item[3],item[4]),tags=("evenrow",))
        else:
            my_tree2.insert(parent="", index=END, iid=count, text="", values=(item[0],item[1],item[2],item[3],item[4]),tags=("oddrow",))
        count += 1

    conn.commit()
    conn.close()
