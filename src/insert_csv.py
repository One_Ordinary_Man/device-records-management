import sqlite3
import csv

f1 = "../data/DRM.db"
f2 = "../data/Log.db"

def insert_phone(p_file):
    conn = sqlite3.connect(f1)
    c = conn.cursor()
    file = open(p_file, 'r')
    file_read = csv.reader(file)

    for item in file_read:
        c.execute("INSERT OR IGNORE INTO Device_phone VALUES(?,?,?,?)",(item[0],item[1],item[2],item[3]))
    
    conn.commit()
    file.close()
    conn.close()

def insert_biometric(b_file):
    conn = sqlite3.connect(f1)
    c = conn.cursor()
    file = open(b_file, 'r')
    file_read = csv.reader(file)

    for item in file_read:
        c.execute("INSERT OR IGNORE INTO Device_biometric VALUES(?,?,?)",(item[0],item[1], item[2]))
    
    conn.commit()
    file.close()
    conn.close()

def insert_sim(s_file):
    conn = sqlite3.connect(f1)
    c = conn.cursor()
    file = open(s_file, 'r')
    file_read = csv.reader(file)

    for item in file_read:
        c.execute("INSERT OR IGNORE INTO Sim VALUES(?,?,?)",(str(item[0]),int(item[1]), item[2]))    
    conn.commit()
    file.close()
    conn.close()

def insert_Employee(e_file):
    conn = sqlite3.connect(f1)
    c = conn.cursor()
    file = open(e_file, 'r')
    file_read = csv.reader(file)

    for item in file_read:
        c.execute("""INSERT OR IGNORE INTO Employee_info 
        VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)""",(item[0], item[1],item[2],item[3], item[4],item[5],item[6], item[7],item[8],item[9], item[10],item[11],item[12], item[13],str(item[14])))
    c.execute("UPDATE Employee_info SET Phone_asset_id = NULL WHERE Phone_asset_id = ''")
    c.execute("UPDATE Employee_info SET Biometric_asset_id = NULL WHERE Biometric_asset_id = ''")
    c.execute("UPDATE Employee_info SET Sim_no = NULL WHERE Sim_no = ''")
    conn.commit()
    file.close()
    conn.close()

