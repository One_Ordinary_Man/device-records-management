import sqlite3
import csv
import os

f1="../data/DRM.db"
f2="../data/Log.db"

ef="../temp/employee.csv"
pf="../temp/phone.csv"
bf="../temp/biometric.csv"
sf="../temp/sim.csv"
plf="../temp/provider_log.csv"
rlf="../temp/retrieval_log.csv"

def Backup():
    conn = sqlite3.connect(f1)
    conn2 = sqlite3.connect(f2)
    c=conn.cursor()
    c2=conn2.cursor()

    c.execute("""SELECT *
            FROM Employee_info""")
    e_i = c.fetchall()

    c.execute("""SELECT *
            FROM Device_Phone""")
    d_p = c.fetchall()

    c.execute("""SELECT *
        FROM Device_Biometric""")
    d_b = c.fetchall()

    c.execute("""SELECT *
        FROM Sim""")
    s = c.fetchall()

    c2.execute("""SELECT *
            FROM Provider_Log""")
    p_l = c.fetchall()

    c2.execute("""SELECT *
            FROM Retrieval_Log""")
    r_l = c.fetchall()

    with open(ef, 'w') as e_file:
        csv_write = csv.writer(e_file)
        for line in e_i:
            csv_write.writerow(line)

    with open(pf, 'w') as p_file:
        csv_write = csv.writer(p_file)
        for line in d_p:
            csv_write.writerow(line)

    with open(bf, 'w') as b_file:
        csv_write = csv.writer(b_file)
        for line in d_b:
            csv_write.writerow(line)

    with open(sf, 'w') as s_file:
        csv_write = csv.writer(s_file)
        for line in s:
            csv_write.writerow(line)

    with open(plf, 'w') as pl_file:
        csv_write = csv.writer(pl_file)
        for line in p_l:
            csv_write.writerow(line)

    with open(rlf, 'w') as rl_file:
        csv_write = csv.writer(rl_file)
        for line in r_l:
            csv_write.writerow(line)
    conn.commit()
    conn2.commit()
    conn.close()
    conn2.close()