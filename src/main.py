# importing packages
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
import sqlite3
import datetime
import Init
import insert_csv
import Gui_Credentials
import Backup

f1 = "../data/DRM.db"
f2 = "../data/Log.db"

#Defining the root window
root = Tk()
root.title("DRM")
root.geometry("1024x720")

#Creating a frame inside the window
frame1 = Frame(root, pady=10)
frame1.pack()

frame2 = Frame(root,pady=10)
frame2.pack()

# reset database function
def init():
    Init.Init()
    success = Label(frame1, text="Success")
    success.grid(row=1,column=0,columnspan=2,pady=10)

# inserting the csv file into database
def insert():
    # creating a new window called tl1
    tl1 = Toplevel()
    tl1.title("Insert")
    tl1.geometry("400x600")
    
    # creating in_frame inside tl1
    in_frame = Frame(tl1, pady=10)
    in_frame.pack()

    # inserting the phone file
    def p_file():
        p_file_f = filedialog.askopenfilename(initialdir=".", title="Phone file", filetypes=(("csv files", "*.csv"),("All Files","*.*")))
        insert_csv.insert_phone(p_file_f)
        confirm_p = Label(in_frame, text="Success")
        confirm_p.grid(row=1,column=0,pady=10)
    
    # inserting the biometric file
    def b_file():
        b_file_f = filedialog.askopenfilename(initialdir=".", title="Biometric file", filetypes=(("csv files", "*.csv"),("All Files","*.*")))
        insert_csv.insert_biometric(b_file_f)
        confirm_b = Label(in_frame, text="Success")
        confirm_b.grid(row=3,column=0,pady=10)

    # inserting the sim file
    def s_file():
        s_file_f = filedialog.askopenfilename(initialdir=".", title="Sim file", filetypes=(("csv files", "*.csv"),("All Files","*.*")))
        insert_csv.insert_sim(s_file_f)
        confirm_s = Label(in_frame, text="Success")
        confirm_s.grid(row=5,column=0,pady=10)

    # inserting the employee_info file
    def e_file():
        e_file_f = filedialog.askopenfilename(initialdir=".", title="Employee file", filetypes=(("csv files", "*.csv"),("All Files","*.*")))
        insert_csv.insert_Employee(e_file_f)
        confirm_e = Label(in_frame, text="Success")
        confirm_e.grid(row=7,column=0,pady=10)
    
    # Creating the labels and buttons
    p_file_l = Label(in_frame, text="Insert the phone file", width=30)
    p_file_l.grid(row=0, column=0,pady=10)

    p_file_b = Button(in_frame,text="Add", command=p_file)
    p_file_b.grid(row=0, column=1,padx=10,pady=10)

    b_file_l = Label(in_frame, text="Insert the biometric file", width=30)
    b_file_l.grid(row=2, column=0,pady=10)

    b_file_b = Button(in_frame,text="Add", command=b_file)
    b_file_b.grid(row=2, column=1,padx=10,pady=10)

    s_file_l = Label(in_frame, text="Insert the sim file", width=30)
    s_file_l.grid(row=4, column=0,pady=10)

    s_file_b = Button(in_frame,text="Add", command=s_file)
    s_file_b.grid(row=4, column=1,padx=10,pady=10)

    e_file_l = Label(in_frame, text="Insert the employee file", width=30)
    e_file_l.grid(row=6, column=0,pady=10)

    e_file_b = Button(in_frame,text="Add", command=e_file)
    e_file_b.grid(row=6, column=1,padx=10,pady=10)

    tl1.mainloop()

def Search():
    global frame2
    frame2.destroy()
    frame2 = Frame(root,pady=10)
    frame2.pack()    
    conn = sqlite3.connect(f1)
    c = conn.cursor()
    c.execute("""SELECT Employee_info.Name, Employee_info.Designation, Employee_info.Office, Employee_info.Phone_asset_id, Device_phone.IMEI_1, Device_phone.IMEI_2, Device_phone.Model, 
    Employee_info.Biometric_asset_id, Device_biometric.Serial_No, Device_biometric.Model, Employee_info.Sim_No, Sim.Mobile_No, Sim.Brand 
    FROM Employee_info 
    LEFT JOIN Device_phone ON Employee_info.Phone_asset_id = Device_phone.Asset_ID 
    LEFT JOIN Device_biometric ON Employee_info.Biometric_asset_id = Device_biometric.Asset_ID 
    LEFT JOIN Sim ON Employee_info.Sim_No = Sim.Sim_No 
    WHERE Employee_info.AMS_ID =? """,(search_entry.get(),))
    list = c.fetchall()

    E_name = Label(frame2, text="Employee Name:"+str(list[0][0]), width=50)
    E_designation = Label(frame2, text="Employee Designation:"+str(list[0][1]),width=50)
    E_office = Label(frame2, text="Employee Office:"+str(list[0][2]), width=50)
    E_pai = Label(frame2, text="Phone Asset Id:"+str(list[0][3]), width=50)
    P_i1 = Label(frame2, text="Phone Imei1:"+str(list[0][4]), width=50)
    P_i2 = Label(frame2, text="Phone Imei2:"+str(list[0][5]), width=50)
    P_ml = Label(frame2, text="Phone Model:"+str(list[0][6]), width=50)
    E_bai = Label(frame2, text="Biometric Asset Id:"+str(list[0][7]), width=50)
    B_sn = Label(frame2, text="Biometric Serial No:"+str(list[0][8]), width=50)
    B_ml = Label(frame2, text="Biometric Model:"+str(list[0][9]), width=50)
    E_sn = Label(frame2, text="Sim no:"+str(list[0][10]), width=50)
    S_mn = Label(frame2, text="Mobile no:"+str(list[0][11]), width=50)
    S_bd = Label(frame2, text="Brand:"+str(list[0][12]), width=50)

    E_name.grid(row=0,column=0,padx=10)
    E_designation.grid(row=1,column=0,padx=10)
    E_office.grid(row=2,column=0,padx=10)
    E_pai.grid(row=3,column=0,padx=10)
    P_i1.grid(row=4,column=0,padx=10)
    P_i2.grid(row=5,column=0,padx=10)
    P_ml.grid(row=6,column=0,padx=10)
    E_bai.grid(row=0,column=1,padx=10)
    B_sn.grid(row=1,column=1,padx=10)
    B_ml.grid(row=2,column=1,padx=10)
    E_sn.grid(row=3,column=1,padx=10)
    S_mn.grid(row=4,column=1,padx=10)
    S_bd.grid(row=5,column=1,padx=10)

    def phone_clear():
        conn = sqlite3.connect(f1)
        conn2 = sqlite3.connect(f2)
        c = conn.cursor()
        c2 = conn2.cursor()
        c.execute("""SELECT AMS_ID, Phone_asset_id
                FROM Employee_info
                WHERE AMS_ID = ?""",(search_entry.get(),))
        p_list = c.fetchone()
        c.execute("""UPDATE Employee_info
                SET Phone_asset_id = NULL
                WHERE AMS_ID = ?""",(search_entry.get(),))
        c.execute("""SELECT IMEI_1, IMEI_2, MODEL
                FROM Device_phone
                WHERE Asset_ID = ?""",(p_list[1],))
        d_p = c.fetchone()
        c2.execute("""INSERT INTO Retrieval_Log
                (Date, AMS_ID, Asset_id, Device)
                VALUES (?,?,?,?)""",(datetime.datetime.now().strftime("%d-%m-%Y"),p_list[0],p_list[1],"P : M-" + str(d_p[2]) + " I1-" + str(d_p[0])+ " I2-" + str(d_p[1])))
        conn.commit()
        conn2.commit()
        conn.close()
        conn2.close()

    def bio_clear():
        conn = sqlite3.connect(f1)
        conn2 = sqlite3.connect(f2)
        c = conn.cursor()
        c2 = conn2.cursor()
        c.execute("""SELECT AMS_ID, Biometric_asset_id
                FROM Employee_info
                WHERE AMS_ID = ?""",(search_entry.get(),))
        b_list = c.fetchone()        
        c.execute("""UPDATE Employee_info
                SET Biometric_asset_id = NULL
                WHERE AMS_ID = ?""",(search_entry.get(),))
        c.execute("""SELECT Serial_No, Model
                FROM Device_biometric
                WHERE Asset_ID = ?""",(b_list[1],))
        d_b = c.fetchone()
        c2.execute("""INSERT INTO Retrieval_Log
                (Date, AMS_ID, Asset_id, Device)
                VALUES (?,?,?,?)""",(datetime.datetime.now().strftime("%d-%m-%Y"),b_list[0],b_list[1],"B : M-"+str(d_b[1])+" S-"+str(d_b[0])))
        conn.commit()
        conn2.commit()
        conn.close()
        conn2.close()

    def sim_clear():
        conn = sqlite3.connect(f1)
        conn2 = sqlite3.connect(f2)
        c = conn.cursor()
        c2 = conn2.cursor()
        c.execute("""SELECT AMS_ID, Sim_No
                FROM Employee_info
                WHERE AMS_ID = ?""",(search_entry.get(),))
        s_list = c.fetchone()
        c.execute("""UPDATE Employee_info
                SET Sim_No = NULL
                WHERE AMS_ID = ?""",(search_entry.get(),))
        c.execute("""SELECT Mobile_No, Brand
                FROM Sim
                WHERE Sim_No = ?""",(s_list[1],))
        d_s = c.fetchone()
        c2.execute("""INSERT INTO Retrieval_Log
                (Date, AMS_ID, Asset_id, Device)
                VALUES (?,?,?,?)""",(datetime.datetime.now().strftime("%d-%m-%Y"),s_list[0],s_list[1],"S : M-"+str(d_s[1])+" S-"+str(d_s[0])))
        conn.commit()
        conn2.commit()
        conn2.close()
        conn.close()

    c_p = Button(frame2,text="Clear Phone", command=phone_clear)
    c_b = Button(frame2,text="Clear Biometric", command=bio_clear)
    c_s = Button(frame2,text="Sim Clear", command=sim_clear)

    c_p.grid(row=7,column=0,pady=20)
    c_b.grid(row=7,column=1,pady=20)
    c_s.grid(row=7,column=2,pady=20)

    c.execute("""SELECT Device_phone.Asset_ID
            FROM Device_phone
            EXCEPT
            SELECT Employee_info.Phone_asset_id
            FROM Employee_info
            WHERE Employee_info.Phone_asset_id IS NOT NULL""")
    p_tup_a = c.fetchall()

    p_list_a = []
    for item in p_tup_a:
        p_list_a.append(item[0])

    def p_link(p_list_a):
        conn = sqlite3.connect(f1)
        conn2 = sqlite3.connect(f2)
        c = conn.cursor()
        c2 = conn2.cursor()

        c.execute("""SELECT Asset_ID
                FROM Device_phone
                WHERE IMEI_1 = ?""",(p_entry.get(),))
        
        p_asset = c.fetchone()

        if p_asset[0] in p_list_a:
                c.execute("""UPDATE Employee_info
                        SET Phone_asset_id = ?
                        WHERE AMS_ID = ?""",(p_asset[0],search_entry.get()))

                c.execute("""SELECT IMEI_1, IMEI_2, MODEL
                FROM Device_phone
                WHERE Asset_ID = ?""",(p_asset[0],))
                d_p = c.fetchone()

                c2.execute("""INSERT INTO Provider_Log
                        (Date, AMS_ID, Asset_id, Device)
                        VALUES (?,?,?,?)""",(datetime.datetime.now().strftime("%d-%m-%Y"), search_entry.get(), p_asset[0], "P : M-" + str(d_p[2]) + " I1-" + str(d_p[0])+ " I2-" + str(d_p[1])))
                p_entry.delete(0,END)
        
        else:
                messagebox.showerror("Error","Error wrong credentials")

        conn.commit()
        conn2.commit()
        conn.close()
        conn2.close()

    c.execute("""SELECT Device_biometric.Asset_ID
            FROM Device_biometric
            EXCEPT
            SELECT Employee_info.Biometric_asset_id
            FROM Employee_info
            WHERE Employee_info.Biometric_asset_id IS NOT NULL""")
    b_tup_a = c.fetchall()

    b_list_a = []
    for item in b_tup_a:
        b_list_a.append(item[0])

    def b_link(b_list_a):
        conn = sqlite3.connect(f1)
        conn2 = sqlite3.connect(f2)
        c = conn.cursor()
        c2 = conn2.cursor()

        c.execute("""SELECT Asset_ID
                FROM Device_biometric
                WHERE Serial_No = ?""",(b_entry.get(),))

        b_serial = c.fetchone()

        if b_serial[0] in b_list_a:
                c.execute("""UPDATE Employee_info
                        SET Biometric_asset_id = ?
                        WHERE AMS_ID = ?""",(b_serial[0],search_entry.get()))
                
                c.execute("""SELECT Serial_No, Model
                FROM Device_biometric
                WHERE Asset_ID = ?""",(b_serial[0],))
                d_b = c.fetchone()

                c2.execute("""INSERT INTO Provider_Log
                        (Date, AMS_ID, Asset_ID,Device)
                        VALUES (?,?,?,?)""",(datetime.datetime.now().strftime("%d-%m-%Y"), search_entry.get(), b_serial[0], "B : M-"+str(d_b[1])+" S-"+str(d_b[0])))
                b_entry.delete(0,END)

        else:
                messagebox.showerror("Error","Error wrong credentials")
        conn.commit()
        conn2.commit()
        conn.close()
        conn2.close()

    c.execute("""SELECT Sim.Sim_No
            FROM Sim
            EXCEPT
            SELECT Employee_info.Sim_No
            FROM Employee_info
            WHERE Employee_info.Sim_No IS NOT NULL""")
    s_tup_a = c.fetchall()

    s_list_a = []
    for item in s_tup_a:
        s_list_a.append(item[0])
    
    def s_link(s_list_a):
        conn = sqlite3.connect(f1)
        conn2 = sqlite3.connect(f2)
        c = conn.cursor()
        c2 = conn2.cursor()
        if s_entry.get() in s_list_a:
                c.execute("""UPDATE Employee_info
                        SET Sim_No = ?
                        WHERE AMS_ID = ?""",(s_entry.get(),search_entry.get()))
                c.execute("""SELECT Mobile_No, Brand
                        FROM Sim
                        WHERE Sim_No = ?""",(s_entry.get(),))
                d_s = c.fetchone()
                c2.execute("""INSERT INTO Provider_Log
                        (Date, AMS_ID, Asset_ID,Device)
                        VALUES (?,?,?,?)""",(datetime.datetime.now().strftime("%d-%m-%Y"), search_entry.get(), s_entry.get(), "S : M-"+str(d_s[1])+" S-"+str(d_s[0])))
                s_entry.delete(0,END)
        
        else:
                messagebox.showerror("Error","Error wrong credentials")

        conn.commit()
        conn2.commit()
        conn.close()
        conn2.close()
    
    

    
    p_entry = Entry(frame2, width=30)
    p_entry.grid(row=8,column=0,pady=10)

    p_but = Button(frame2, text="Link phone", command=lambda: p_link(p_list_a))
    p_but.grid(row=9,column=0,pady=10)
 
    b_entry = Entry(frame2, width=30)
    b_entry.grid(row=8,column=1,pady=10)

    b_but = Button(frame2, text="Link biometric", command=lambda: b_link(b_list_a))
    b_but.grid(row=9,column=1,pady=10)
 
    s_entry = Entry(frame2, width=30)
    s_entry.grid(row=8,column=2,pady=10)

    s_but = Button(frame2, text="Link Sim", command=lambda: s_link(s_list_a))
    s_but.grid(row=9,column=2,pady=10)


    conn.commit()
    conn.close()

# creating labels and buttons inside the root frame
insert_label = Label(frame1, text="Insert CSV files", width=30)
insert_label.grid(row= 0, column= 2, pady=10)

insert_button = Button(frame1, text= "Insert", command=insert)
insert_button.grid(row=0, column=3, padx=10, pady=10)

init_label = Label(frame1, text="Reset Database", width=30)
init_label.grid(row= 0, column= 0, pady=10)

init_button = Button(frame1, text="Reset", command=init)
init_button.grid(row=0, column=1, padx=10, pady=10)

showdb_label = Label(frame1,text="Show Database", width=30)
showdb_label.grid(row=2, column=0,pady=10)

showdb_button = Button(frame1, text="Show", command=Gui_Credentials.show)
showdb_button.grid(row=2,column=1,padx=10,pady=10)

showl_label = Label(frame1,text="Show Log", width=30)
showl_label.grid(row=2, column=2,pady=10)

showl_button = Button(frame1, text="Show", command=Gui_Credentials.log)
showl_button.grid(row=2,column=3,padx=10,pady=10)

search_label = Label(frame1, text="AMS ID:", width=30)
search_label.grid(row=4,column=1,pady=10)

search_entry = Entry(frame1, width=30)
search_entry.grid(row=4,column=2,pady=10)

search_Button = Button(frame1, text="Search", command=Search)
search_Button.grid(row=4,column=3, pady=10)

backup_label = Label(frame1,text="Backup", width=30)
backup_label.grid(row=3, column=0,pady=10)

backup_button = Button(frame1, text="Backup", command=Backup.Backup)
backup_button.grid(row=3,column=1,padx=10,pady=10)

root.mainloop()
