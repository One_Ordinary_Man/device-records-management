import sqlite3
from os.path import exists
import os

f1="../data/DRM.db"
f2="../data/Log.db"

def Init():
    if exists(f1):    
        os.remove(f1)
    
    if exists(f2):
        os.remove(f2)

    conn = sqlite3.connect(f1)
    c = conn.cursor()

    c.execute("""CREATE TABLE IF NOT EXISTS Device_phone(
        Asset_ID TEXT PRIMARY KEY,
        IMEI_1 INTEGER NOT NULL,
        IMEI_2 INTEGER NOT NULL,
        Model TEXT NOT NULL
    )""")

    c.execute("""CREATE TABLE IF NOT EXISTS Device_biometric(
        Asset_ID TEXT PRIMARY KEY,
        Serial_No TEXT NOT NULL,
        Model TEXT NOT NULL
    )""")

    c.execute("""CREATE TABLE IF NOT EXISTS Sim(
        Sim_No TEXT PRIMARY KEY,
        Mobile_No INTEGER NOT NULL,
        Brand TEXT NOT NULL
    )""")

    c.execute("PRAGMA foreign_keys = ON")

    c.execute("""CREATE TABLE IF NOT EXISTS Employee_info(
        AMS_ID TEXT PRIMARY KEY,
        AMS_Status TEXT,
        Name TEXT,
        DOB TEXT,
        Emp_ID INTEGER,
        Designation TEXT,
        Office TEXT,
        Facility_ID TEXT,
        MDM_ID TEXT,
        Reporting_office TEXT,
        Sub_Division TEXT,
        Retirement_Date TEXT,
        Phone_asset_id TEXT,         
        Biometric_asset_id TEXT,
        Sim_No TEXT,
        FOREIGN KEY (Phone_asset_id) REFERENCES Device_phone(Asset_ID),
        FOREIGN KEY (Biometric_asset_id) REFERENCES Device_biometric(Asset_ID),
        FOREIGN KEY (Sim_No) REFERENCES Sim(Sim_No)
        )""")

    conn.commit()
    conn.close()

    conn2 = sqlite3.connect(f2)
    c = conn2.cursor()
    c.execute("""CREATE TABLE IF NOT EXISTS Retrieval_Log(
            Reciept_no INTEGER PRIMARY KEY AUTOINCREMENT,
            Date TEXT NOT NULL,
            AMS_ID INTEGER NOT NULL,
            Asset_id TEXT NOT NULL,
            Device TEXT NOT NULL
            )""")
    c.execute("""CREATE TABLE IF NOT EXISTS Provider_Log(
            Reciept_no INTEGER PRIMARY KEY AUTOINCREMENT,
            Date TEXT NOT NULL,
            AMS_ID INTEGER NOT NULL,
            Asset_id TEXT NOT NULL,
            Device TEXT NOT NULL            
            )""")
    conn2.commit()
    conn2.close()
